<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Data')->insert([
            'data' => $this->generate_random_json_data(),
            'access' => true,
        ]);
        DB::table('Data')->insert([
            'data' =>  $this->generate_random_json_data(),
            'access' => false,
        ]);
    }

    private function generate_random_json_data($count = 9)
    {
        //неохото искать готовое решение
        //Лучше было бы написать с помощью фабрик, используя $faker, но задание есть задание
        $array = [];

        for ($i = 1; $i <= 9; $i++):
            $array[rand(1, 1000)] =  Str::random(10);
        endfor;

        $array = json_encode($array);
        return $array;
    }
}
