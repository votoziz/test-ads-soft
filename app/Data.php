<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'Data';
    public $timestamps = false;

    protected $fillable = [
        'data',
        'access'
    ];
}
