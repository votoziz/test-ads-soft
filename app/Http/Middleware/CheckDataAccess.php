<?php

namespace App\Http\Middleware;

use Closure;

class CheckDataAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $data = json_decode($response->getContent());

        if (!isset($data->access)) {
            return response()->json(['error' => 'Data not found'], 404);
        }

        if (!$data->access) {
            return response()->json(['error' => 'Do not have access'], 403);
        }

        return $response;
    }
}
