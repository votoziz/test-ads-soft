
# Тестовое задание для "АДС-Софт"

Изотов Виктор 89524909753 votoziz@gmail.com

Проект собран с использованием phpsthorm

## Выполняемые команды во время разработки

php artisan make:migration create_data_table --create=Data

php artisan make:model Data

php artisan migrate

php artisan make:seeder DataTableSeeder

php artisan db:seed

php artisan make:auth

php artisan make:controller Api/DataController --resource

php artisan make:middleware CheckDataAccess
