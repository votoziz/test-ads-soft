@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-primary text-white text-center">
                    Формы vue.js</div>

                <div class="card-body">


                    <show_data-component></show_data-component>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
